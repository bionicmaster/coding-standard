# Introduction

Entrepids Coding Standard is a Magento-friendly version of the [FIG PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md) coding standard intended for use with the [PHP_CodeSniffer (PHPCS)](https://github.com/squizlabs/PHP_CodeSniffer) tool by Squiz Labs. It incorporates the most of the built-in PSR-2 coding standard with the [Magento Coding Standard](https://github.com/magento-ecg/coding-standard) created by ECG and a sniff "borrowed" from the [WordPress Coding Standards](https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/) (the sniff ensures that the class name is in the correct format). Please note that some rules, such as the requirement for namespaces to be used, have been removed from the PSR-2 coding standard to make it compatible with Magento, a lot of elements where taken from MagePSR-2 and others from Made.com rules

# Downloading
```
https://bitbucket.org/bionicmaster/coding-standard
```

# Prerequisites

1. PHP version 5.4 or greater
2. PEAR or Composer rules
3. Plugin for your IDE (@see IDEs supported at Entrepids)
4. [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)

# Installation

Clone the repository to the path of your choice:
cd /path/to/CodeSniffer/Standards
or cd ~/coding-standards (or cd %USERPROFILE%\coding-standards on Windows)
git clone https://bitbucket.org/bionicmaster/coding-standard.git Entrepids/
```

Alternatively, add a dependency to your project's `composer.json` file:
```json
{
    "require": {
        "bionicmaster/coding-standard": "dev-master"
    }
}
```

# Usage

Run PHP_CodeSniffer:
```
phpcs --standard=/path/to/Entrepids /path/to/code
...

NETBEANS
---
Download at plugins PHPCSMD
Go to Tools | Options | PHP | PHPCSMD
Activate PHPCS
Input the path of your phpcs (or phpcs.bat on Windows)
select on standard Entrepids (only if you selected the path/to/Codesniffer path)

PHPSTORM
---
Go to Settings | Editor | Inspections
Checkbox at PHP | PHP Codesniffer Validation
Select at the right side, Custom as standard and select the path of your Entrepids repository


ECLIPSE
---
Go to PHP Tools | Library | PHP Codesniffer
Select your PHP and Pear Paths
Add our new rule from New and select our Entrepids Custom Standard