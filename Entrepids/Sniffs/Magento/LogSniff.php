<?php

/**
 * Class Entrepids_Sniffs_Magento_LogSniff
 *
 * Discourages the use of Mage::log()
 *
 * @category PHP
 * @package PHP_Codesniffer_Entrepids
 */
class Entrepids_Sniffs_Magento_LogSniff implements PHP_CodeSniffer_Sniff
{
    /**
     * If true error will be thrown, else will be a warning
     *
     * @var bool
     */
    public $error = true;

    /**
     * Registers the tokens that this sniff wants to listen for.
     *
     * @return int[]
     * @see    Tokens.php
     */
    public function register()
    {
        return array(T_DOUBLE_COLON);
    }

    /**
     * Called when one of the token types that this sniff is listening for
     * is found.
     *
     * The stackPtr variable indicates where in the stack the token was found.
     * A sniff can acquire information this token, along with all the other
     * tokens within the stack by first acquiring the token stack:
     *
     * <code>
     *    $tokens = $phpcsFile->getTokens();
     *    echo 'Encountered a '.$tokens[$stackPtr]['type'].' token';
     *    echo 'token information: ';
     *    print_r($tokens[$stackPtr]);
     * </code>
     *
     * If the sniff discovers an anomaly in the code, they can raise an error
     * by calling addError() on the PHP_CodeSniffer_File object, specifying an error
     * message and the position of the offending token:
     *
     * <code>
     *    $phpcsFile->addError('Encountered an error', $stackPtr);
     * </code>
     *
     * @param PHP_CodeSniffer_File $phpcsFile The PHP_CodeSniffer file where the
     *                                        token was found.
     * @param int $stackPtr The position in the PHP_CodeSniffer
     *                                        file's token stack where the token
     *                                        was found.
     *
     * @return void
     */
    public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();

        $prevToken = $tokens[$stackPtr - 1];
        $nextToken = $tokens[$stackPtr + 1];

        if ($prevToken['content'] == 'Mage' && $nextToken['content'] == 'log') {
            $phpcsFile->addWarning('Mage::log() debugging is discouraged', $stackPtr, 'Discouraged');
        }
    }
}